<?php

if ( post_password_required() ) {
    return;
}
?>

<div id="comments" class="comments-area">

    <?php if ( have_comments() ) : ?>
        <h2 class="comments-title">
            <?php
                printf(
                    _nx( 'One thought on &ldquo;%2$s&rdquo;',
                        '%1$s thoughts on &ldquo;%2$s&rdquo;',
                        get_comments_number(),
                        'comments title',
                        'cvitae' ),
                number_format_i18n( get_comments_number() ), get_the_title() );
            ?>
        </h2>

        <?php cvitae_comment_nav();?>

        <ol class="comment-list">
            <?php
            wp_list_comments( array(
                'style'       => 'ol',
                'short_ping'  => true,
                'avatar_size' => 56,
            ) );
            ?>
        </ol><!-- .comment-list -->

        <?php cvitae_comment_nav();?>

    <?php endif; // have_comments() ?>

    <?php
        // If comments are closed and there are comments, let's leave a little note, shall we?
        if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
    ?>
        <p class="no-comments"><?php _e( 'Comments are closed.', 'cvitae' ); ?></p>
    <?php endif; ?>

    <?php comment_form(); ?>

</div><!-- .comments-area -->
<?php

function cvitae_comment_nav(){?>

    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // check for comment navigation ?>

        <div class="col-sm-12">

            <nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">

                <ul class="pager">

                    <li class="nav-previous">
                        <?php previous_comments_link( __( '&larr; Older Comments', 'cvitae' ) ); ?>
                    </li>

                    <li class="nav-next">
                        <?php next_comments_link( __( 'Newer Comments &rarr;', 'cvitae' ) ); ?>
                    </li>

                </ul>

            </nav><!-- #comment-nav-below -->

        </div>

    <?php endif; // Check for comment navigation. ?>

<?php }
