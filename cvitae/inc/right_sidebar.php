<?php if(is_active_sidebar('right_sidebar')){?>
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="sidebar">
                <?php
                    if ( ! dynamic_sidebar('right_sidebar') ) :
                        get_search_form();
                    endif;
                ?>

            </div>
        </div>
    </div>
<?php } ?>