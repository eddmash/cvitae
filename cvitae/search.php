<?php get_header();?>

<div class="container">
    <?php get_template_part("inc/menu");?>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="search-info">
                <h2>
                    <?php printf( __( 'Results for: %s', "cvitae" ), '<em>' . get_search_query() . '</em>'); ?>
                </h2>
                <?php get_search_form();?>
            </div>
            <div class="single_posts">
                <!--            The loop -->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div <?php post_class('single_post'); ?> >

                        <div class="single_post_title">
                            <h2>
                                <a href="<?php the_permalink(); ?>"
                                   title="<?php the_title_attribute( 'before=Permalink to: "&after="' ); ?>" >
                                    <?php the_title();?>
                                </a>
                            </h2>
                        </div>

                        <?php get_template_part("inc/post_meta");?>

                        <div class="single_post_excerpt">
                            <?php the_excerpt();?>
                        </div>

                    </div>
                <?php endwhile;?>
<!--                    After all the posts have been looped-->
                    <?php get_template_part("inc/pagination")?>
                <?php else : ?>
                    <p class="single_post">
                        <?php
                            printf(__( 'Sorry, no results matched the search term `%1s`', 'cvitae'),get_search_query());
                        ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-2">
            <?php get_template_part("inc/right_sidebar");?>
        </div>
    </div>
</div>


<?php get_footer();?>