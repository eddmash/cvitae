<div class="row">
    <div class="col-sm-12">
        <div class="credits_social_media clearfix">
            <ul class="social_media">

                <?php if(get_theme_mod("facebook_visible")){?>
                    <li>
                        <a href="http://<?php echo get_theme_mod("facebook");?>" target="_blank">
                            <i class="fa fa-facebook-official"></i> Facebook

                        </a>
                    </li>
                <?php } ?>

                <?php if(get_theme_mod("twitter_visible")){?>
                    <li>
                        <a href="http://<?php echo get_theme_mod("twitter");?>" target="_blank">
                            <i class="fa fa-twitter"></i> Twitter
                        </a>
                    </li>
                <?php } ?>


                <?php if(get_theme_mod("googleplus_visible")){?>
                    <li>
                        <a href="http://<?php echo get_theme_mod("googleplus");?>" target="_blank">
                            <i class="fa fa-google-plus"></i> Googe+
                        </a>
                    </li>
                <?php } ?>


                <?php if(get_theme_mod("mail_visible")){?>
                    <li>
                        <a href="http://<?php echo get_theme_mod("mail");?>" target="_blank">
                            <i class="fa fa-envelope"></i> Mail
                        </a>
                    </li>
                <?php } ?>


                <?php if(get_theme_mod("rss_visible")){?>
                    <li>
                        <a href="http://<?php echo get_theme_mod("rss");?>" target="_blank">
                            <i class="fa fa-rss"></i> rss
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php if(get_theme_mod("credits_visible")){?>
                <span class="credits pull-right">
                    <small>
                        <strong><?php get_theme_mod("credits_display_message")?></strong>
                        <a href="<?php get_theme_mod("credits_url")?>"><?php get_theme_mod("credits_name")?></a>
                    </small>
                </span>
            <?php } ?>
        </div>
    </div>
</div>