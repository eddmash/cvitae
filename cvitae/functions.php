<?php
/**
 * Created by PhpStorm.
 * User: eddmash
 * Date: 10/8/15
 * Time: 2:51 PM
 */



function cvitae_setup(){
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'automatic-feed-links' );
    $header_img = array(
        'width'=> 500,
        'height'=> 500,
        'default-image'=>get_stylesheet_directory_uri().'/assets/img/profile_default.png'
    );
    add_theme_support( 'custom-header',$header_img);
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

    if ( ! isset( $content_width ) ) {
        $content_width = 600;
    }


    // sanitize inputs
    function cvitae_clean_input_data($data){
        $data = sanitize_text_field($data);
        return $data;
    }
    function cvitae_front_page_settings($wp_customize){
        $wp_customize->add_section("front_page", array(
            'title' => __( 'Front Page Details', 'cvitae'),
            'description' => __( 'Set some fields that are displayed on the front page', 'cvitae'),
            'panel' => '', // Not typically needed.
            'priority' => 160,
            'capability' => 'edit_theme_options',
        ));
        $text_settings = array("text_before_image", "text_after_image", "greetings");

        foreach($text_settings as $setting) {
            $setting_name = ucwords(str_replace("_"," ", $setting));
            $wp_customize->add_setting($setting.'_visible',
                array(
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport' => 'refresh',
                    'sanitize_callback'=>'cvitae_clean_input_data')
            );

            $wp_customize->add_setting($setting,
                array(
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport' => 'refresh',
                    'sanitize_callback'=>'cvitae_clean_input_data')
            );


            $wp_customize->add_control($setting.'_visible',
                array(
                    'type'=>'checkbox',
                    'priority' => 11,
                    'label'=>'Enable '.$setting_name,
                    'section'=>'front_page')
            );

            $wp_customize->add_control($setting,
                array(
                    'type'=>'text',
                    'priority' => 11,
                    'label'=>$setting_name,
                    'section'=>'front_page')
            );
        }


    }
    // social media settings
    function cvitae_social_media_settings($wp_customize){
        $wp_customize->add_section("social_media", array(
            'title' => __( 'Social Media', 'cvitae'),
            'description' => __( 'Add social media urls', 'cvitae'),
            'capability' => 'edit_theme_options',
        ));
        $social_settings = array("facebook", "twitter", "googleplus", "mail", "rss");

        foreach($social_settings as $social){
            $desc = 'www.'.$social.'.com/username';
            if($social == "mail"){
                $desc = "example@mail.com";
            }
            // visible settings
            $wp_customize->add_setting($social."_visible",
                array(
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport' => 'refresh',
                    'sanitize_callback'=>'cvitae_clean_input_data'
                )
            );
            $wp_customize->add_control($social.'_visible',
                array(
                    'type'=>'checkbox',
                    'label'=>'Enable '.$social,
                    'section'=>'social_media')
            );
            // actual settings
            $wp_customize->add_setting($social,
                array(
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport' => 'refresh',
                    'sanitize_callback'=>'cvitae_clean_input_data'
                )
            );

            $wp_customize->add_control($social,
                array(
                    'type'=>'text',
                    'label'=>$social,
                    'description' =>sprintf(__("enter the complete URL e.g. %1s", "cvitae"),$desc),
                    'section'=>'social_media')
            );
        }

    }

    // credit settings
    function cvitae_credits_settings($wp_customize){
        $credits = array("credits_website", "credits_display_name", "credits_display_message");
        $credits_visible = array("credits_visible", "on_front_page_visible");

        $wp_customize->add_section("credits", array(
            'title' => __( 'Site Credits', 'cvitae'),
            'description' => __( 'Add the site credits here', 'cvitae'),
            'capability' => 'edit_theme_options',
        ));

        foreach($credits as $credit){
            // actual settings
            $wp_customize->add_setting($credit,
                array(
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport' => 'refresh',
                    'sanitize_callback'=>'cvitae_clean_input_data'
                )
            );

            $wp_customize->add_control($credit,
                array(
                    'type'=>'text',
                    'label'=>$credit,
                    'description' =>sprintf(__("provide  %1s", "cvitae"),$credit),
                    'section'=>'credits')
            );
        }
        foreach($credits_visible as $credit){
            $credit_name = ucwords(str_replace("_"," ", $credit));
            // actual settings
            $wp_customize->add_setting($credit,
                array(
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport' => 'refresh',
                    'sanitize_callback'=>'cvitae_clean_input_data'
                )
            );

            $wp_customize->add_control($credit,
                array(
                    'type'=>'checkbox',
                    'label'=>$credit_name,
                    'section'=>'credits')
            );
        }
    }

    function cvitae_custom_excerpt_length( $length ) {
        return 60;
    }

    function cvitae_excerpt_more( $more ) {
        return '<em><a class="read-more" href="'.get_permalink(get_the_ID()).'">'.
        __( '&nbsp;...&nbsp;read more', 'cvitae' ).'</a></em>';
    }

    function cvitae_register_my_menu() {
        register_nav_menu('main_menu',__( 'Main Menu', 'cvitae'));
    }

    function cvitae_editor_styles() {
        add_editor_style();
    }

    function cvitae_register_widgets(){
        $args = array(
            'name' => __( 'Right Sidebar', 'cvitae' ),
            'id' => 'right_sidebar',
            'description' => __( 'Displays widgets on the right side', 'cvitae' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title"><h4 class="widgettitle">',
            'after_title'   => '</h4></div>',
        );
        register_sidebar($args);
    }

    function cvitae_comment_form($args) {
        $commenter = wp_get_current_commenter();
        $req = get_option( 'require_name_email' );
        $args['fields'] = array(
            'author' =>
                '<div class="form-group">
                    <label for="author">'.__("Name","cvitae").'</label>
                    <input id="author"
                            name="author" type="text" value="' . __( 'Your Name ...', 'cvitae' ) .'"
                            size="30"' . ( $req ? " aria-required='true'" : '' ) . '
                            class="form-control"
                            placeholder="' . __( 'Your Name', 'cvitae' ) . ( $req ? '*' : '' ) . '" /></div>',

                'email' =>
                    '<div class="form-group">
                    <label for="email">'.__("Email","cvitae").'</label>
                    <input id="email"
                            name="email" type="text" value="' . __( 'Your Email ...', 'cvitae' ) .'"
                            size="30"' . ( $req ? " aria-required='true'" : '' ) . '
                            class="form-control"
                            placeholder="' . __( 'Your Email', 'cvitae' ) . ( $req ? '*' : '' ) . '" /></div>',
        );
        $args['comment_notes_before'] = "";
        $args['comment_notes_after'] = "";
        $args['label_submit'] = "Submit";
        $args['class_submit'] = "btn btn-primary";
        $args['title_reply'] = __("Leave a Comment","cvitae");
        $args['comment_field'] = '<div class="form-group">
                                    <label for="comment">'.__("Message","cvitae").'</label>
                                    <textarea id="comment"
                                            name="comment"
                                            aria-required="true"
                                            class="form-control"
                                            placeholder="'. __( 'Your Message here...', 'cvitae' ) .'"
                                            tabindex="4"></textarea></div>';

        return $args;
    }

    add_filter('comment_form_defaults', 'cvitae_comment_form');

    add_action( 'admin_init', 'cvitae_editor_styles' );
    add_action( 'widgets_init', 'cvitae_register_widgets' );
    add_action('init', 'cvitae_register_my_menu' );
    add_filter('excerpt_more', 'cvitae_excerpt_more' );
    add_filter('excerpt_length', 'cvitae_custom_excerpt_length');
    add_action("customize_register", "cvitae_social_media_settings");
    add_action("customize_register", "cvitae_front_page_settings");
    add_action("customize_register", "cvitae_credits_settings");

}

cvitae_setup();


function cvitae_bootstrap_assets(){
    wp_enqueue_style( 'bootstrapCSS', get_stylesheet_directory_uri().'/assets/css/bootstrap.min.css');
    wp_enqueue_style( 'fontAwesomeCSS', get_stylesheet_directory_uri().'/assets/css/font-awesome.min.css');
    wp_enqueue_script( 'bootstrapJS', get_stylesheet_directory_uri().'/assets/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri()."/style.css");

}

add_action( 'wp_enqueue_scripts', 'cvitae_bootstrap_assets' );
