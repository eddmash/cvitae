<?php get_header();?>

<div class="container">
    <?php get_template_part("inc/menu");?>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="single_posts">
                <!--            The loop -->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div <?php post_class('single_post'); ?> >

                        <div class="single_post_title clearfix">
                            <h2>

                                <?php the_title();?>
                                <?php if(get_edit_post_link()){?>
                                    <small class="pull-right edit_post_link">
                                        <a href="<?php echo get_edit_post_link()?>">
                                            <i class="fa fa-edit fg_teal"></i> Edit
                                        </a>
                                    </small>
                                <?php } ?>
                            </h2>
                        </div>

                        <div class="single_post_content">
                            <?php the_content();?>

                            <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                }
                            ?>
                        </div>

                        <div class="single_post_pagination">
                            <?php wp_link_pages(); ?>
                        </div>
                    </div>

                <?php endwhile; ?>
                    <?php get_template_part("inc/pagination-single")?>
                <?php else :?>

                <p><?php _e( 'Sorry, no posts matched your criteria.', 'cvitae' ); ?></p>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<?php get_footer();?>