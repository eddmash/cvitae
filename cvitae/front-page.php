<?php get_header();?>

    <div class="container full-height">
        <div class="row full-height">
            <div class="col-sm-12">
                <div class="intro">

                    <div class="text-center" id="owner_name">
                        <span><?php echo bloginfo("title");?></span>
                    </div>

                    <div class="text-center" id="headshot">
                        <em>
                            <strong>
                                <?php
                                    if(!get_theme_mod("text_before_image")){
                                        _e("text before image", "cvitae");
                                    }
                                    if(get_theme_mod("text_before_image_visible")){
                                        echo get_theme_mod("text_before_image");
                                    }
                                ?>
                                <?php if(get_theme_mod("text_before_image_visible")){ ?>
                                    <i class="fa fa-arrow-right fg_orange hidden-xs"></i>
                                    <i class="fa fa-arrow-down fg_orange visible-xs"></i>
                                <?php } ?>

                            </strong>

                        </em>
                        <!--   hide on small screens-->
                        <em class="photograph">
                            <?php if(get_header_image()){?>
                                <img src="<?php echo get_header_image();?>"
                                     class="headshot_img img-responsive img-circle"
                                     alt="<?php echo(get_bloginfo('title')); ?>"/>
                            <?php }else{?>
                                <img src="<?php echo get_stylesheet_directory_uri().'/assets/img/profile_default.png'?>"
                                     class="headshot_img img-responsive img-circle"
                                     alt="<?php echo(get_bloginfo('title')); ?>"/>
                            <?php } ?>
                        </em>
                        <em>
                            <strong>
                                <?php
                                    if(!get_theme_mod("text_after_image")){
                                        _e("text after image", "cvitae");
                                    }
                                    if(get_theme_mod("text_after_image_visible")) {
                                        echo get_theme_mod("text_after_image");
                                    }
                                ?>
                            </strong>
                        </em>
                    </div>

                    <div class="text-center" id="greeting_message">
                        <span>
                            <?php
                                if(get_theme_mod("greetings_visible")) {
                                    echo get_theme_mod("greetings");
                                }
                            ?>
                        </span>
                    </div>
                </div>

                <div id="owner_description" class="text-center">
                    <p> <?php echo bloginfo("description");?></p>
                </div>


                <?php get_template_part("inc/menu");?>
                <?php get_template_part("inc/credits_social_media");?>
            </div>



        </div>
    </div>




</body>

</html>