<div class="clearfix post_meta_data">
    <ul class="post_meta">

        <li class="post_date">
            <span class="fg_teal"><i class="fa fa-calendar"></i> Date</span>
            <?php
                $archive_year  = get_the_time('Y');
                $archive_month = get_the_time('m');
                $archive_day   = get_the_time('d');

                echo '<a href="'.get_month_link($archive_year, $archive_month, $archive_day).'">'.
                    get_the_time('M').'</a>&nbsp;';

                echo '<a href="'.get_day_link($archive_year, $archive_month, $archive_day).'">'.
                    get_the_time('j').'</a>,&nbsp;';

                echo '<a href="'.get_year_link($archive_year, $archive_month, $archive_day).'">'.
                    get_the_time('Y').'</a>';

            ?>

        </li>

        <li class="bypostauthor">
            <span class="fg_teal"><i class="fa fa-user"></i> By</span>
            <?php the_author_posts_link();?>
        </li>

        <?php if(get_edit_post_link() && is_single()){?>
            <li class="pull-right edit_post_link">
                <a href="<?php echo get_edit_post_link()?>">
                    <i class="fa fa-edit fg_teal"></i> Edit
                </a>
            </li>
        <?php } ?>

    </ul>
</div>
