<div class="row">
    <div class="col-sm-12">
        <nav class="navbar navbar-default main_menu">
            <div class="navbar-header text-center">
                <button type="button"
                        class="navbar-toggle collapsed"
                        data-toggle="collapse" data-target="#the_navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="the_navigation">
                <?php

                        $defaults = array(
                            'container' => false,
                            'theme_location'=>'main_menu',
                            'menu_class'=>'nav nav-pills',
                            'menu_id'=>'menu_items'
                        );

                        wp_nav_menu($defaults);
                ?>
            </div>
        </nav>
    </div>
</div>