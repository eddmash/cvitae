<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label class="control-label sr-only">
        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'cvitae' ) ?></span>
    </label>

    <div class="input-group">

        <input type="search"
               class="search-field form-control"
               placeholder="<?php echo esc_attr_x( 'Search ...', 'placeholder', 'cvitae') ?>"
               value="<?php echo get_search_query() ?>"
               name="s"
               title="<?php echo esc_attr_x( 'Search for:', 'label', 'cvitae' ) ?>" />

        <span class="input-group-btn">

            <button class="btn btn-default" type="submit">Go!</button>
        </span>
    </div>
</form>